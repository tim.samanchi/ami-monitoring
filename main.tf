resource "aws_cloudwatch_metric_alarm" "ami_creation_date_alarm" {
  alarm_name          = "AMI_Creation_Date_Alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "60"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "SampleCount"
  threshold           = "5"
  actions_enabled = true
  alarm_description   = "Triggered when the AMI creation date is less than the threshold"
  alarm_actions       = []

  dimensions = {
    "ImageId" = "i-0a3fdb74f8ef26609"
  }

  tags = {
    Name        = "AMI Creation Date Alarm"
    Environment = "Production"
  }
}


#    ImageId = "ami-08529cc3cfc109dd7"
